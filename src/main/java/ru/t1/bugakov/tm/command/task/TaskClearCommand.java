package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}
